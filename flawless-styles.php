<?php

/**
 * @package Flawless Styles
 * @version 0.1
 */
/*
Plugin Name: Flawless Styles
Plugin URI: http://peterrknight.com/plugins/flawless-styles/
Description: A powerful UI for writing and modifying CSS for WordPress themes, individual pages and components.
Author: Peter Knight
Version: 0.1
Author URI: http://peterrknight.com/
*/

define( 'FLAWLESS_STYLES_LIB_PATH', dirname( __FILE__ ) . '/lib/' );


require_once( FLAWLESS_STYLES_LIB_PATH . 'on-demand-util/boot.php' );

/**
 * TODO REMOVE THIS AFTER DEBUGGING
 */
add_action( 'init', 'stop_heartbeat', 1 );
function stop_heartbeat() {
	if( isset( $_SERVER['HTTP_HOST'] )
	 && ( strpos( $_SERVER['HTTP_HOST'], '.dev' ) !== false
	    || strpos( $_SERVER['HTTP_HOST'], '.local' ) !== false ) ){
		wp_deregister_script('heartbeat');
	}
}

function flawless_customizer_includes() {
	require_once( dirname( __FILE__ ) . '/includes/customizer/customize.php' );

	on_demand_utils_boot();
}

add_action('wp_ajax_prk_localized_scripts_on_demand', 'flawless_customizer_includes', 0 );
/*
function flawless_init_customizer( $components ) {
	flawless_customizer_includes();
	$instance = \PRK\CustomizerEnvironments\customizer_environment_manager();

	return $instance->filter_customize_loaded_component( $components );
}*/

require_once( FLAWLESS_STYLES_LIB_PATH . 'customizer-environment-manager/boot.php' );
customizer_environments_boot();

add_action( 'customizer_environment_registrations', 'flawless_register_customizer_environment' );

function flawless_register_customizer_environment( $customizer_environments ) {
	flawless_customizer_includes();
	$customizer_environments->register_environment(
		'flawless',
		array( 'class' => 'Flawless_Styles_Customizer' )
	);
}

function flawless_customize_instance() {
	static $instance;
	if ( is_null( $instance ) && is_user_logged_in() ) {
		\flawless_customizer_includes();
		$manager = \PRK\CustomizerEnvironments\customizer_environment_manager();
		if ( isset( $manager->environment ) && $manager->environment === 'flawless' ) {
			$instance = $manager->env_class;
		} else {
			$instance = new Flawless_Styles_Customizer();
		}
	}

	return $instance;
}

/**
 * This plugin can be packaged inside other plugins which may not need all functionality; by default we only load
 * all of the features when it's used a standalone plugin or if the parent plugin asks for it (by setting a constant)
 *
 * These features include a dedicated flawless_stylesheet post_type and boot logic so that the stylesheet can applied to
 * all or parts of a site.
 *
 * This allows plugins to only use Flawless_Styles to help compile styles while letting them organise when and how the
 * styles are shown, assigning the styles to different post_types and objects instead without the need for a separate
 * post type and without the need for separate boot logic.
 */
if( ( defined( "FLAWLESS_FULL_STRAP" ) && FLAWLESS_FULL_STRAP )
    || 'flawless-styles/flawless-styles.php' === plugin_basename( __FILE__ )  ){
	require_once( dirname( __FILE__ ) . '/includes/standalone-boot.php' );
}
