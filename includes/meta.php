<?php


function flawless_misc_meta(){

	// boot mode: theme-override|complimentary
	// load mode inline|critical|default
	// critical css


}

function flawless_styles_meta( $post_type ){
	delight_me('meta_reg', $post_type );
	if( did_action( 'flawless_meta_registration' ) ){
		return;
	}

	$args = array(
		//'sanitize_callback' => 'sanitize_my_meta_key',
		'auth_callback' => '__return_true',
		'type'          => 'string',
		'single'        => true,
		'show_in_rest'  => true,
		//'object_subtype'    => $post_type,
	);

	register_post_meta(
		$post_type,
		'_flawless_routes',
		$args
	);

	register_post_meta(
		$post_type,
		'_flawless_load_opts',
		$args
	);

	register_post_meta(
		$post_type,
		'_flawless_custom_css',
		$args
	);

	register_post_meta(
		$post_type,
		'_flawless_custom_css_compiled',
		$args
	);

	register_post_meta(
		$post_type,
		'_flawless_css_version_status',
		$args
	);

	register_post_meta(
		$post_type,
		'_flawless_custom_css_optimized',
		$args
	);


	register_post_meta(
		$post_type,
		'flawless_css_version_status',
		$args
	);


	register_post_meta(
		$post_type,
		'_flawless_custom_css_minified',
		$args
	);

	register_post_meta(
		$post_type,
		'_flawless_color_scheme',
		array(
			'auth_callback'     => '__return_true',
			'type'              => 'string',
			'description'       => 'Page specific css variables',
			'single'            => true,
			'show_in_rest'      => true,
			'sanitize_callback' => 'sps_test_nested'
		)
	);


	register_post_meta(
		$post_type,
		'_flawless_typography',
		$args
	);

	do_action( 'flawless_meta_registration' );
}

function flawless_stylesheet_version( $object_id ){
	$version = wp_parse_args(
		get_post_meta( $object_id, '_flawless_css_version_status', true ),
		array( 'production_version' => 0, 'dev_version' => 0 )
	);
	return $version;
}

function flawless_is_css_key( $key ){
	$keys = array(
		'_flawless_color_scheme',
		'_flawless_typography',
		'_flawless_custom_css',
	);
	return( in_array( $key, $keys  ) );
}

add_action('flawless_css_dev_version_increment', 'flawless_increment_dev_version', 10, 3 );
function flawless_increment_dev_version($object_id, $meta_key, $_meta_value){
	$version = flawless_stylesheet_version( $object_id );
	$version['dev_version'] += 1;
	update_post_meta( $object_id, '_flawless_css_version_status', $version );
}

function flawless_on_updated_css( $meta_id, $object_id, $meta_key, $_meta_value ){
	delight_me( 'updated_meta_' . $meta_key, get_defined_vars() );
	if( flawless_is_css_key(  $meta_key ) && ! did_action( 'flawless_css_dev_version_increment' ) ){
		do_action( 'flawless_css_dev_version_increment', $object_id, $meta_key, $_meta_value );
	}

	if( '_flawless_custom_css_optimized' !== $meta_key ) {
		return;
	}
	$minified = fsc_minify_css( $_meta_value );
	$fsc_compiler = fsc_get_compiler();
	$path = flawless_upload_path( 'flawless' );
	$file_created = $fsc_compiler->save_to_file( $minified, $path, $object_id . '.css');
	$version = flawless_stylesheet_version( $object_id );
	$version['production_version'] = $version['dev_version'];
	$update_version = update_post_meta( $object_id, '_flawless_css_version_status', $version );
	flawless_updated_stylesheet_version($version['production_version']);
	return update_post_meta( $object_id, '_flawless_custom_css_minified', $minified );
}
add_action( 'updated_post_meta', 'flawless_on_updated_css', 10, 4 );

function flawless_on_updated_typography( $meta_id, $object_id, $meta_key, $_meta_value ){
	// we're interested in _flawless_typography because it stores google font names that we selected

	if( '_flawless_typography' !== $meta_key ) {
		return;
	}

	// let's update _sps_font_selection which we use to create instructions to include font requests
	$font_selections = array();
	// let's weed out empty values first
	foreach( $_meta_value as $var => $font_family ){
		if( ! empty( $font_family ) ){
			$font_selections[] = $font_family;
		}
	}

	$fonts = (array) get_post_meta( $object_id, '_sps_font_selection' );
	$new_font_sets = array();
	$fonts = array();
	foreach( $fonts as $font ){
		if( $font['source'] === 'google_font' ){
			$font_pos = array_search( $font['source'], $font_selections );
			if( $font_pos === false ){

			} else {
				$new_font_sets[] = $font;
				unset( $font_selections[ $font_pos ] );
			}
		} else {
			$new_font_sets[] = $font;
		}
	}

	foreach( $font_selections as $var => $font_selection ){
		$new_font_sets[] = array( 'source' => 'google', 'fontfamily' => $font_selection );
	}

	return update_post_meta( $object_id, '_flawless_font_selection', $new_font_sets );
}
add_action( 'updated_post_meta', 'flawless_on_updated_typography', 10, 4 );
