<?php
delight_me('compat_loaded', did_action( 'make_view_loaded' ) );
// optimising make theme integration
add_action( 'make_view_loaded', function(){
	remove_action(
		'wp_head',
		array( Make()->get_module('style'), 'get_styles_as_inline' ),
		11
	);
});


