<?php

/**
 * Turn off theme features when we're using Flawless Stylesheets to manage css and fonts
 *
 */

function flawless_theme_compat_mode( $theme ){
	if( in_array( $theme, flawless_theme_compat_list() ) ){
		delight_me('theme_compat', $theme, dirname( __FILE__ ) . '/' . $theme . '/compat.php' );
		require_once( dirname( __FILE__ ) . '/' . $theme . '/compat.php' );
	}
}

function flawless_theme_compat_list(){
	return array( 'make' );
}