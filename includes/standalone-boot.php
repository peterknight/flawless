<?php

require_once( FLAWLESS_STYLES_LIB_PATH . 'on-demand-util/boot.php' );
require_once( 'admin-bar.php' );
require_once( 'post-type.php' );
require_once( 'meta.php' );
require_once( 'font-loading.php' );

require_once( FLAWLESS_STYLES_LIB_PATH . '/post-manager/boot.php' );
prk_post_manager_boot();


add_action( 'init', 'flawless_styles_meta_reg' );

function flawless_styles_meta_reg() {
	flawless_styles_meta( 'flawless_stylesheet' );
}


function flawless_shown_theme( $supplied_theme = false ) {
	static $theme;
	if ( is_null( $theme ) && $supplied_theme ) {
		$theme = $supplied_theme;
	}
	delight_me( 'theme_data', get_post_custom( $theme['ID'] ) );
	return $theme;
}


function flawless_stylesheet_uri( $id ) {
	require_once( FLAWLESS_STYLES_LIB_PATH . '/flawless-styles-compiler/boot.php' );

	return flawless_upload_uri( 'flawless/' ) . $id . '.css';
}

function flawless_maybe_load_stylesheet() {

	if( wp_doing_ajax() ){
		return false;
	}

	if(  defined( 'XMLRPC_REQUEST' ) || defined( 'REST_REQUEST' ) && REST_REQUEST ){
		return false;
	}

	$cache              = flawless_stylesheet_cacher();
	$sheets             = $cache->get_all();
	$candidate          = false;
	$candidate_priority = 0;

	global $pagenow;
	if ( $pagenow === 'post.php' ) {
		if( isset( $_REQUEST['post']) ){
			$id = $_GET['post'];
			$active_path = get_permalink( $id );
		} else {
			return false;
		}


	} else {
		$active_path        = $_SERVER['REQUEST_URI'];
	}
	foreach ( $sheets as $sheet => $data ) {
		$priority = flawless_load_priority( $active_path, $data );
		delight_me( 'candidatetr', 'yes', get_defined_vars() );
		if ( $priority > $candidate_priority ) {
			$candidate = $sheet;
			$candidate_priority = $priority;
		} else if( $priority === $candidate_priority &&
		           strlen( $candidate['meta']['_flawless_routes'] ) < $sheet['meta']['_flawless_routes'] ){
			$candidate = $sheet;
			$candidate_priority = $priority;
		}
	}
	delight_me( 'candidate', 'yes', get_defined_vars() );
	if ( $candidate ) {
		flawless_shown_theme( $sheets[ $candidate ] );
		add_action( 'wp_enqueue_scripts', 'flawless_enqueue_fonts' );
		add_action( 'wp_enqueue_scripts', 'flawless_stylesheet_loader', 99999 );
		add_action( 'enqueue_block_editor_assets', 'flawless_enqueue_fonts' );
		add_action( 'enqueue_block_editor_assets', 'flawless_stylesheet_loader', 99999 );


		add_action( 'wp_print_styles', 'flawless_dequeue_theme_styles' );
		require_once( dirname(__FILE__) . '/theme-compat/theme-compat.php' );

		delight_me( 'new_theme_data', wp_get_theme()->get_template() );
		flawless_theme_compat_mode( wp_get_theme()->get_template() );

	}
	add_filter( 'stylesheet_uri', 'flawless_filter_stylesheet', 10, 2 );
}

add_action( 'init', 'flawless_post_monitor' );

function flawless_enqueue_stylesheet(){

}

function flawless_maybe_monitor() {
	if ( is_user_logged_in() && current_user_can( 'edit_pages' ) ) {
		flawless_post_monitor();
	}
}

add_action( 'init', 'flawless_maybe_load_stylesheet' );


function flawless_stylesheet_loader() {

	$active_stylesheet = flawless_shown_theme();

	if( ! $active_stylesheet ){
		return;
	}

	if (  ! is_customize_preview() ) {
		if( isset( $active_stylesheet['meta']['_flawless_css_version_status'] ) ){
			$version = $active_stylesheet['meta']['_flawless_css_version_status']['production_version'];
		} else {
			$version = time();
		}
		wp_enqueue_style(
			'active-flawless-stylesheet',
			flawless_stylesheet_uri( $active_stylesheet['ID'] ),
			array(),
			$version
		);

	}
}

function flawless_boot_stylesheet(){

}

function flawless_dequeue_theme_styles() {
	$parent     = get_template_directory_uri();
	$child      = get_stylesheet_directory_uri();
	$has_parent = ( $parent !== $child );

	global $wp_styles;

	foreach ( $wp_styles->queue as $i => $handle ) {
		if ( isset( $wp_styles->registered[ $handle ] ) ) {
			$dequeue = false;
			$src     = $wp_styles->registered[ $handle ]->src;
			if ( $has_parent ) {
				$pos     = strpos( $wp_styles->registered[ $handle ]->src, $parent );
				$dequeue = ( $pos !== false );
			}
			if ( ! $dequeue ) {
				$pos     = strpos( $wp_styles->registered[ $handle ]->src, $child );
				$dequeue = ( $pos !== false );
			}
			if ( $dequeue ) {
				unset( $wp_styles->queue[ $i ] );
				//wp_dequeue_style( $handle );
			}
		}
	}
}

function flawless_filter_stylesheet( $stylesheet_uri, $stylesheet_dir ) {
	$active_stylesheet = flawless_shown_theme();
	if ( $active_stylesheet ) {
		$stylesheet_uri = flawless_stylesheet_uri( $active_stylesheet['ID'] );
	}
	//delight_me( 'filter_flawless_styleheet', get_defined_vars() );
	return $stylesheet_uri;
}

function flawless_load_priority( $active_path, $data ) {
	$route = $data['meta']['_flawless_routes'];
	$pos   = strpos( $active_path, $route );
	if ( $pos === false ) {
		return 0;
	}
	if ( $pos >= 1 ) {
		$priority = substr_count( $route, '/' ) + 1;
	} else {
		$priority = 1;
	}

	return $priority;
}

add_action( 'on_stylesheet_updated', 'flawless_registered_stylesheets' );


function flawless_post_monitor() {

	static $monitor;

	if ( is_null( $monitor ) ) {
		$monitor = new PRK_Post_Monitor( 'flawless_stylesheet' );
		$cacher  = flawless_stylesheet_cacher();
		$list    = $cacher->get_id_list();
		$monitor->monitor( $list, 'flawless_stylesheet', true, true );

		add_action( 'flawless_stylesheet_event', 'flawless_stylesheet_sync', 10, 2 );
		add_action( 'flawless_stylesheet_deleted', 'flawless_remove_stylesheet', 9, 1 );
		add_action( 'save_post_flawless_stylesheet', 'flawless_add_newly_inserted_stylesheet', 10, 3 );
	}

	return $monitor;
}


function flawless_stylesheet_cacher() {
	static $cacher;

	if ( is_null( $cacher ) ) {
		$cacher = new PRK_Post_Item_Cache( 'flawless_stylesheets', array(
			'index_key'     => 'post_id',
			'post_fields'   => array( 'post_id', 'post_name' ),
			'post_type'     => 'flawless_stylesheet',
			'meta_fields'   => array(
				'_flawless_routes', '_flawless_load_opts', '_flawless_typography', '_flawless_css_version_status'
			),
			'tax_fields'    => false,
			'version'       => '1.0.2',
			'seed_callback' => 'flawless_stylesheets_seed'
		) );
	}

	return $cacher;
}

function flawless_stylesheet_sync( $id, $post ) {
	$cache = flawless_stylesheet_cacher();
	$cache->schedule_update( $id );
}

function flawless_stylesheet_deleted( $id ) {
	$cache = flawless_stylesheet_cacher();
	$cache->remove_item( $id );
}

function flawless_add_newly_inserted_stylesheet( $id, $post, $update ) {
	if ( 'flawless_stylesheet' !== $post->post_type ) {
		return;
	}
	if ( empty( $update ) ) {

		$cacher = flawless_stylesheet_cacher();
		// adds the item to the cache/config
		$cacher->update_item( $id );

		$monitor = flawless_post_monitor();
		// watches changes to the template
		$monitor->monitor( $id, 'flawless_stylesheet' );
	} else if ( $post->post_status === 'publish' ) {
		$cacher = flawless_stylesheet_cacher();
		// adds the item to the cache/config
		$cacher->update_item( $id );

	}
}

function flawless_stylesheets_seed() {
	$args  = array(
		'post_type'      => 'flawless_stylesheet',
		'post_status'    => array( 'publish' ),
		'posts_per_page' => - 1,
		'fields'         => 'ids'
	);
	$query = new WP_Query( $args );
	if ( is_array( $query->posts ) ) {
		return $query->posts;
	} else {
		return array();
	}
}

// Add backend styles for Gutenberg.
add_action( 'enqueue_block_editor_assets', 'flawless_add_gutenberg_assets' );

/**
 * Load Gutenberg stylesheet.
 */
function flawless_add_gutenberg_assets() {
	// Load the theme styles within Gutenberg.
	//wp_enqueue_style( 'flawless-gutenberg', get_theme_file_uri( '/assets/css/gutenberg-editor-style.css' ), false );
	delight_me( 'stylehseets', get_post_custom( 123 ), get_posts( array( 'post_type' => 'flawless_stylesheet' ) ) );
}