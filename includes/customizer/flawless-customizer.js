/* global wp, jQuery */
/* exported CustomizerBlankSlate */

var flawlessCustomizer = (function ( api, $, _ ) {
    'use strict';
    //console.log(flawlessCustomizeVars);
    var component = {
        data: {
            queryParamName: null,
            queryParamValue: null,
            secondQueryParamName: null,
            secondQueryParamValue: null
        }
    };

    /**
     * Initialize functionality.
     *
     * @param {object} args Args.
     * @param {string} args.queryParamName  Query param name.
     * @param {string} args.queryParamValue Query param value.
     * @returns {void}
     */
    component.init = function init( args ) {
        _.extend( component.data, args );
        if ( !args || !args.queryParamName || !args.queryParamValue ) {
            throw new Error( 'Missing args' );
        }

        api.bind( 'ready', function () {
            component.injectPreviewUrlQueryParam();
            component.cssVarManager();
            component.saveActions();
        } );


    };

    /**
     * Make sure that all previewed URLs include our special customizer query param.
     *
     * @returns {void}
     */
    component.injectPreviewUrlQueryParam = function injectPreviewUrlQueryParam() {
        var previousValidate = api.previewer.previewUrl.validate;
        api.previewer.previewUrl.validate = function injectQueryParam( url ) {
            var amend = false, queryString, queryParams = {}, urlParser, validatedUrl;
            validatedUrl = previousValidate.call( this, url );

            // Parse the query params.
            urlParser = document.createElement( 'a' );
            urlParser.href = validatedUrl;
            queryString = urlParser.search.substr( 1 );

            // store all the found query parameters in queryParams object
            _.each( queryString.split( '&' ), function ( pair ) {
                var parts = pair.split( '=', 2 );
                if ( parts[ 0 ] ) {
                    if( _.isUndefined( parts[ 1 ] ) ){
                        queryParams[ decodeURIComponent( parts[ 0 ] ) ] = null;
                    } else {
                        queryParams[ decodeURIComponent( parts[ 0 ] ) ] = decodeURIComponent( parts[ 1 ] );
                    }
                    //queryParams[ decodeURIComponent( parts[ 0 ] ) ] = _.isUndefined( parts[ 1 ] ) ? null : decodeURIComponent( parts[ 1 ] );
                }
            } );

            // Amend the query params if our first param not set.
            if ( component.data.queryParamValue !== queryParams[ component.data.queryParamName ] ) {
                queryParams[ component.data.queryParamName ] = component.data.queryParamValue;
                amend = true;
            }
            // Amend the query params if second param is desired and not present.
            if ( component.data.secondQueryParamName
                && component.data.secondQueryParamName !== queryParams[ component.data.secondQueryParamName ] ) {
                queryParams[ component.data.secondQueryParamName ] = component.data.secondQueryParamValue;
                amend = true;
            }

            if( amend ){
                urlParser.search = $.param( queryParams );
                validatedUrl = urlParser.href;
            }
            return validatedUrl;
        };
    };

    component.getFieldID = function ( field ) {
        if ( flawlessCustomizeVars.fields[ field ] ) {
            return flawlessCustomizeVars.fields[ field ];
        }
        return false;
    };


    component.cssVarManager = function () {
        api.bind( "change", function ( control ) {

            console.log( 'changed control', control(), control );
        } );
        api.control.each( function ( control, values ) {
            console.log(
                control.id,
                flawlessCustomizeVars.cssVars[ control.id ],
                'addingControlLogic'
            );
            console.log( values );


            if ( flawlessCustomizeVars.cssVars[ control.id ] ) {

                component.updateCSSVars(
                    flawlessCustomizeVars.cssVars[ control.id ][ 0 ],
                    control.setting.get(),
                    flawlessCustomizeVars.cssVars[ control.id ][ 1 ]
                );
                var doNothing = function ( value ) {
                    console.log( 'doing nothing', value );
                };
                // api.control('change', doNothing );

                control.setting.unlink();
                control.setting.bind( function ( newValue, oldValue ) {
                    console.log( newValue, oldValue );
                    component.updateCSSVars(
                        flawlessCustomizeVars.cssVars[ control.id ][ 0 ],
                        newValue,
                        flawlessCustomizeVars.cssVars[ control.id ][ 1 ]
                    );

                } );
                /*control.bind("change", function(value){
                 console.log('changed value', value);
                 });*/
            }
        } );
    };

    component.updateCSSVars = function ( variable, value, group ) {
        var groupControl = api.control( flawlessCustomizeVars.groups[ group ] );
        console.log( groupControl, group );
        var data = JSON.parse( JSON.stringify( groupControl.setting.get() ) );
        console.log( data );
        if ( !data ) {
            data = {};
        }
        data[ variable ] = value;
        groupControl.setting.set( data );
        component.hydrateVars();
    };


    component.refreshVars = function () {
        console.log( 'hello' );
        var cssVars = {};
        _.each( flawlessCustomizeVars.groups, function ( group, control ) {
            console.log( group, control, api.control( group ).setting.get() );
            _.extend( cssVars, api.control( group ).setting.get() );
        } );
        console.log( 'newVARs', cssVars );
        api.previewer.send( 'refresh-css-vars', cssVars );
    };

    component.hydrateVars = _.debounce( component.refreshVars, 100 );

    component.saveActions = function () {
        wp.customize.bind( 'changeset-save', function ( data ) {
            console.log( 'CHANGESETSAVE', data );
            _.each( data, function ( key, value ) {
                console.log( key, value );
            } );

        } );

        wp.customize.bind( 'change', function ( data ) {
            /*console.log('BEFORESAVING', data());
             if( typeof data() === 'object' ){
             if( data())
             } */
            //data().text_background_color = '#333333';
        } );

        wp.customize.bind( 'changeset-saved', function ( data ) {
            console.log( 'CHANGESETSAVEdddd!!!!', data );


        } );
    }

    component.freshCopy = function( data ){
        //if( )
        window.testtest = data;
        console.log( 'freshcopy', data, data.length, JSON.parse( JSON.stringify( data ) ), JSON.parse( JSON.stringify( data ) ).length );
        return JSON.parse( JSON.stringify( data ) );
    };

    component.slugify = function(str) {
        const from  = "ąàáäâãåæćęęèéëêìíïîłńòóöôõøśùúüûñçżź",
            to    = "aaaaaaaaceeeeeeiiiilnoooooosuuuunczz",
            regex = new RegExp('[' + from.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1') + ']', 'g');

        if (str === null) return '';

        str = String(str).toLowerCase().replace(regex, function(c) {
            return to.charAt(from.indexOf(c)) || '-';
        });

        return str.replace(/[^\w\s-]/g, '').replace(/([A-Z])/g, '-$1').replace(/[-_\s]+/g, '-').toLowerCase();
    };

    return component;

}( wp.customize, jQuery, _ ) );