<?php

add_action ( 'flawless_settings_registration', 'flawless_css_variable_settings', 10, 3 ); // $wp_customize, object_id
add_action ( 'flawless_settings_registration', 'flawless_nested_css_setting_registration', 10, 3 );
//add_action( 'flawless_settings_registration', 'flawless_nested_css_setting_registration', 10, 3 );
add_action ( 'flawless_controls_registration', 'flawless_css_variable_controls', 10, 3 );

function flawless_css_variable_controls( $wp_customize, $post_id, $flawless ) {

	//$css_var_registration = flawless_customizer();

	$groups = array(
		'color_scheme' => array(
			'title'    => __( 'Color Scheme' ),
			'meta_key' => '_flawless_color_scheme'
		),
		'typography'   => array(
			'title'    => __( 'Typography' ),
			'meta_key' => '_flawless_typography'
		)
	);

	$variables = flawless_nested_css_setting_registration( $wp_customize, $post_id );

	$group_settings = flawless_css_variable_settings( $wp_customize, $post_id );


	foreach ( $groups as $group_name => $config ) {
		$settings = array(
			'panel'           => 'flawless-panel',
			'capability'      => 'edit_posts',
			'active_callback' => '__return_true'
		);
		$args     = wp_parse_args( $config, $settings );
		$wp_customize->add_section( "flawless_{$group_name}", $args );


	}

	foreach ( $groups as $group => $config ) {
		$settings = array(
			'settings'        => $group_settings[ $group ]->id,
			'section'         => "sps_{$group}",
			'capability'      => 'edit_posts',
			'label'           => $config['title'],
			'active_callback' => '__return_true',
			'default'         => 'yes',
		);
		$args     = wp_parse_args( $config, $settings );

		$control = $wp_customize->add_control( new SPS_Customize_Var_Group(
			$wp_customize, $group, $args
		) );

		$controls[] = $control;

		//$css_var_registration->add_group( $group, $control->id );
		$flawless->add_group( $group, $control->id );

	}

	$controls        = array();
	$control_classes = apply_filters( 'flawless_control_classes', array() );

	foreach ( $variables as $control => $config ) {
		$group = $config['group'];
		$id    = SPS_Customize_Nested_Setting::create_setting_id(
			$post_id, $groups[ $group ]['meta_key'], $control
		);

		//$css_var_registration->add_variable( $id, $control, $group );
		$flawless->add_variable( $id, $control, $group );

		$control_class = $control_classes[ $config['control_type'] ];
		$settings      = array(
			'settings'        => $id, //$settings->id
			'section'         => "flawless_{$config['group']}",
			'capability'      => 'edit_posts',
			'active_callback' => '__return_true',
			'default'         => 'yes'
		);
		$args          = wp_parse_args( $config, $settings );

		$controls[] = $wp_customize->add_control( new $control_class(
			$wp_customize, $id, $args
		) );
	}

	return $controls;
}


function flawless_nested_css_setting_registration( $wp_customize, $post_id ) {
	$variables = apply_filters( 'flawless_css_variables', array(), $post_id );
	$groups    = array(
		'color_scheme' => array(
			'title'    => __( 'Color Scheme' ),
			'meta_key' => '_flawless_color_scheme'
		),
		'typography'   => array(
			'title'    => __( 'Typography' ),
			'meta_key' => '_flawless_typography'
		)
	);

	foreach ( $variables as $control => $config ) {
		$group = $config['group'];
		$id    = SPS_Customize_Nested_Setting::create_setting_id(
			$post_id, $groups[ $group ]['meta_key'], $control
		);
		$wp_customize->add_setting(
			new SPS_Customize_Nested_Setting(
				$wp_customize,
				$id,
				array(
					'post_id'   => $post_id,
					'meta_key'  => $groups[ $group ]['meta_key'],
					'key'       => $control,
					'transport' => 'postMessage',
				)
			)
		);
	}

	return $variables;
}


add_filter( 'flawless_css_variables', 'flawless_css_variables_list', 9, 1 );

function flawless_css_variables_list( $variables ) {
	$vars = array(
		'text_color'            => array(
			'label'        => __( 'Text Color' ),
			'group'        => 'color_scheme',
			'control_type' => 'colorpicker',
			'default'      => '#333333',
			'description'  => '@text_color'
		),
		'text_background_color' => array(
			'label'        => __( 'Background Color (for text)' ),
			'group'        => 'color_scheme',
			'control_type' => 'colorpicker',
			'default'      => '',
			'description'  => '@text_background_color'
		),
		'color1'                => array(
			'label'        => 'Primary Color',
			'group'        => 'color_scheme',
			'control_type' => 'colorpicker',
			'default'      => '',
			'description'  => '@color1'
		),
		'color2'                => array(
			'label'        => 'Secondary Color',
			'group'        => 'color_scheme',
			'control_type' => 'colorpicker',
			'default'      => '',
			'description'  => '@color2'
		),
		'color3'                => array(
			'label'        => __( 'Tertiary Color' ),
			'group'        => 'color_scheme',
			'control_type' => 'colorpicker',
			'default'      => '',
			'description'  => '@color3'
		),
		'color4'                => array(
			'label'        => __( '4th Color' ),
			'group'        => 'color_scheme',
			'control_type' => 'colorpicker',
			'default'      => '',
			'description'  => '@color4'
		),
		'font_family_1'         => array(
			'group'        => 'typography',
			'control_type' => 'fontfamily',
			'default'      => '',
			'description'  => '@font_family_1'
		),
		'font_family_2'         => array(
			'group'        => 'typography',
			'control_type' => 'fontfamily',
			'default'      => '',
			'description'  => '@font_family_2'
		),
		'font_family_3'         => array(
			'group'        => 'typography',
			'control_type' => 'fontfamily',
			'default'      => '',
			'description'  => '@font_family_3'
		),
	);

	return wp_parse_args( $variables, $vars );
}


function flawless_css_variable_settings( $wp_customize, $post_id ) {
	$settings = array();
	//$wp_customize->register_control_type( 'SPS_Customize_Var_Group' );
	$groups = array(
		'color_scheme' => array(
			'title'    => __( 'Color Scheme' ),
			'meta_key' => '_flawless_color_scheme'
		),
		'typography'   => array(
			'title'    => __( 'Typography' ),
			'meta_key' => '_flawless_typography'
		)
	);

	foreach ( $groups as $setting => $config ) {
		$id = SPS_Customize_Setting::create_setting_id(
			$post_id, $config['meta_key']
		);

		$settings[ $setting ] = $wp_customize->add_setting(
			new SPS_Customize_Setting(
				$wp_customize,
				$id,
				array(
					'post_id'   => $post_id,
					'meta_key'  => $config['meta_key'],
					'transport' => 'postMessage',
				)
			)
		);
	}

	return $settings;
}


