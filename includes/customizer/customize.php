<?php

require_once( 'class-flawless-styles-customizer.php' );
//require_once( FLAWLESS_STYLE_LIB_PATH . '/customizer-settings-for-postmeta/boot.php' );


add_filter( 'flawless_control_classes', 'flawless_default_control_classes' );

function flawless_default_control_classes( $classes ) {
	$classes = array(
		'colorpicker' => 'WP_Customize_Color_Control',
		'fontfamily'  => 'Flawless_FontFamily_Control',
	);

	return $classes;
}


require_once( FLAWLESS_STYLES_LIB_PATH . 'customizer-settings-for-postmeta/boot.php' );
sps_customize_settings_boot();

require_once( FLAWLESS_STYLES_LIB_PATH . 'flawless-styles-compiler/boot.php' );


require_once( 'customizer-css.php' );
require_once( 'customizer-css-variables.php' );
require_once( 'customizer-sheet-options.php' );
require_once( 'customizer-css-actions.php' );

function flawless_custom_css_control_registration( $wp_customize ) {
	require_once( FLAWLESS_STYLES_LIB_PATH . 'sps-codemirror/boot.php' );
	require_once( FLAWLESS_STYLES_LIB_PATH . 'super-font-selector/boot.php' );
	require_once( FLAWLESS_STYLES_LIB_PATH . 'super-html-gen/boot.php' );
	require_once( FLAWLESS_STYLES_LIB_PATH . 'tabbed-workspaces/boot.php' );
	// Load our custom control.
	require_once( 'controls/class-flawless-customize-css-control.php' );
	require_once( 'controls/class-flawless-customize-color-control.php' );
	require_once( 'controls/class-flawless-customize-var-group.php' );
	require_once( 'controls/class-flawless-customize-css-compiled-control.php' );
	require_once( 'controls/class-flawless-customize-css-optimized-control.php' );
	require_once( 'controls/class-flawless-fontfamily-control.php' );
	// Register the control type.
	/*$wp_customize->register_control_type( 'Flawless_Customize_CSS_Control' );
	$wp_customize->register_control_type( 'Flawless_Customize_Color_Control' );
	$wp_customize->register_control_type( 'Flawless_Customize_CSS_Compiled_Control' );
	$wp_customize->register_control_type( 'Flawless_Customize_CSS_Optimized_Control' );
	$wp_customize->register_control_type( 'Flawless_FontFamily_Control' );*/
	//$wp_customize->register_control_type( 'SPS_Customize_Var_Group' );
	//$wp_customize->register_control_type( 'WP_Customize_Color_Control' );
}