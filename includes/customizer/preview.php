<?php

add_action( 'flawless_preview_init', 'flawless_preview_less', 10, 3 );


function flawless_preview_less( $wp_customize, $id, $flawless ){
	flawless_shown_theme( $id );
	$compiler = fsc_get_compiler();
	$compiler->load_lessjs();
	flawless_variables_json( $id );
	add_action( 'wp_head', 'flawless_load_theme_less' );
}

function flawless_variables_json( $object_id ) {
	$typography   = (array) get_post_meta( $object_id, '_flawless_typography', true  );
	foreach( $typography as $variable => $config ){
		//$config
		if( strpos( $variable, 'font_family' ) === 0 ){
			// make sure we wrap the font names in quotes for css parsing
			$typography[$variable] = "'". $config . "'";
		}
	}
	$color_scheme = (array) get_post_meta( $object_id, '_flawless_color_scheme', true );
	$variables    = array_merge( $typography, $color_scheme );
	$list = flawless_css_variables_list( $variables );

	foreach( $list as $variable => $config ){
		if( empty( $variables[ $variable ] ) && isset( $config['default'] ) ){
			$variables[$variable] = $config['default'];
		}
	}
	delight_me( 'variables', $variables, wp_json_encode( $variables ) );
	wp_add_inline_script( 'lessjs',
		sprintf( 'var startCssVars = %s;', wp_json_encode( $variables ) ),
		'after'
	);
}

function flawless_print_less_style_element( $less, $attrs  = array() ) {
	echo sprintf(
		'<style class="sps-custom-less" type="text/x-less">%s</style>',
		$less
	);
}

add_filter('flawless_custom_css', 'flawless_default_css' );

function flawless_default_css( $css ){
	if( empty( $css ) ){
		$css = array( array( 'group' => 'main', 'css' => 'body {}' ) );
	}
	return $css;
}

function flawless_load_theme_less( $id = false ) {
	$theme = flawless_shown_theme();
	if( empty( $theme ) || is_wp_error( $theme ) ){
		return;
	}
	$id = $theme['ID'];
	$styles = apply_filters(
		'flawless_custom_css',
		(array) get_post_meta( $id, '_flawless_custom_css', true ),
		$id
	);
	$less   = '';
	foreach ( $styles as $group => $data ) {
		if( ! isset( $data['css'] ) ){
			delight_me( 'problem_with', $styles, $group, $data );
			continue;
		}
		$less .= $data['css'];
	}
	flawless_print_less_style_element( $less );
}

function flawless_custom_css_styles( $instance ) {
	$styles = (array) $instance->get_raw_page_field( '_sps_custom_css' );
	$less   = '';
	foreach ( $styles as $group => $data ) {
		$less .= $data['css'];
	}
	flawless_print_less_style_element($less);
	//echo sprintf( '<style class="sps-custom-less" type="text/x-less">%s</style>', $less );

}

