<?php

add_filter( 'flawless_customize_fields', 'flawless_version_field', 10, 3 );

function flawless_version_field( $fields, $post_id, $post ) {

	$fields['flawless-version-status-css'] = SPS_Customize_Setting::create_setting_id(
		$post_id,
		'_flawless_css_version_status'
	);

	/*$fields['block_order'] = SPS_Customize_Setting::create_setting_id(
		$post_id,
		'_sps_block_order'
	);*/

	return $fields;
}

add_action( 'flawless_settings_registration', 'flawless_version_settings', 10, 2 );

function flawless_version_settings( $wp_customize , $post_id ) {

	$settings = array();

	$settings['flawless-css-version-status'] = $wp_customize->add_setting(
		new SPS_Customize_Setting(
		$wp_customize,
		SPS_Customize_Setting::create_setting_id( $post_id, '_flawless_css_version_status' ),
		array(
			'type'            => 'sps_meta',
			'active_callback' => '__return_true',
			'default'         => array( 'dev_version' => 1, 'production_version' => 1 ),
			'transport'       => 'postMessage',
		)
	) );

	$id    = SPS_Customize_Nested_Setting::create_setting_id(
		$post_id,'_flawless_css_version_status', 'production_version'
	);
	$settings['flawless-css-production-version'] = $wp_customize->add_setting(
		new SPS_Customize_Nested_Setting(
			$wp_customize,
			$id,
			array(
				'post_id'   => $post_id,
				'meta_key'  => '_flawless_css_version_status',
				'key'       => 'compiled_version',
				'transport' => 'postMessage',
			)
		)
	);

	$id    = SPS_Customize_Nested_Setting::create_setting_id(
		$post_id,'_flawless_css_version_status', 'dev_version'
	);

	$settings['flawless-css-dev-version'] = $wp_customize->add_setting(
		new SPS_Customize_Nested_Setting(
			$wp_customize,
			$id,
			array(
				'post_id'   => $post_id,
				'meta_key'  => '_flawless_css_version_status',
				'key'       => 'dev_version',
				'transport' => 'postMessage',
			)
		)
	);

	return $settings;
}

function flawless_actions_controls(){


}


add_filter( 'customize_save_response', 'flawless_save_response', 10, 1 );

function flawless_save_response( $response ){
	if( flawless_updated_stylesheet_version() ){
		$css_version = flawless_updated_stylesheet_version();
		$response['flawless_css_version_status'] = array( 'production_version' => $css_version );
	}
	return $response;
}

function flawless_updated_stylesheet_version( $new_version = false ){
	static $version = false;
	if( $new_version ){
		$version = $new_version;
	}
	return $version;
}