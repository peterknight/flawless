/**
 * Preview logic for CSS & Font related controls
 */
(function($, api, flawless){
    /*window.addEventListener('message', function(e) {
        var message = e.data;
        console.log('PREVIEWER RECEIVED MESSAGE', message, e);
    });*/

    api.Events.bind('flawless-compiled-less', function(){
       console.log('compiled');
    });

    api.Events.bind('flawless-font-request', function(data){
        console.log('fontrequest', data);
        WebFont.load({
            google: {
                families: data,
            }
        });
    });

    $(document).ready(function(){

        var invalidCss = false;
        var field = flawless.getFieldID('custom-css');
        var $customLess = $('.sps-custom-less');
        var lessInput = $customLess.html();
        var maybeInvalid = _.debounce( cssWarning, 3000 );
        var queueUpdate = _.debounce(reRender, 100);
        var latestError = false;
        var cssVars =  startCssVars;

        console.log( 'loaded previewscript', field);
        function cssWarning(){
            if( invalidCss ){
                console.log( 'invalid css');
                api.preview.send( 'flawless-compiled-less-error', latestError );
            }
        }

        function reRender(){

            console.log( 'RERENDER', lessInput, cssVars );
            //console.log( less.parse( newval ) );
            less.render(lessInput, 	{ globalVars: cssVars } ).then(function(result){
                console.log( result );
                invalidCss = false;
                $customLess.html(result.css);
                api.preview.send( 'flawless-compiled-less', result.css );
                //console.log( result.toCSS() );
            }).catch(function(e){
                latestError = e;
                invalidCss = true;
                maybeInvalid();
            });
        }

        if( field ) {
            api( field, function ( value ) {
                console.log('appenin', field, api.settings, this.topics);
                value.bind( function ( newval ) {
                    console.log('okay');
                    console.log( newval );
                    if( typeof newval === "string" ){
                        lessInput = newval;
                    } else {
                        lessInput = $.map(newval, function(data){
                          return data['css'];
                        }).join('');
                    }
                   // alert( JSON.stringify( lessInput ) );
                    queueUpdate();
                } );
            } );


            api.preview.bind('refresh-css-vars', function(e){
                console.log('refreshedVARS2', e);
                cssVars = e;
                //less.modifyVars(cssVars);
                queueUpdate();
            });

            api.preview.bind('flawless-font-request', function(data){
                console.log('fontrequest', data, WebFont);
                WebFont.load({
                    google: {
                        families: [data],
                    }
                });
            });
        }
        less.modifyVars(cssVars);
        //reRender();
    });

})(jQuery, wp.customize, flawlessCustomizerPreview );