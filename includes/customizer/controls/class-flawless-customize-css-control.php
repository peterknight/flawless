<?php

class Flawless_Customize_CSS_Control extends WP_Customize_Control {
	//public $type = 'flawless-custom-css';

	/*public function to_json() {
		parent::to_json();
		$this->json['link']    = $this->get_link();
		$this->json['value']   = $this->value();
	}*/

	public function enqueue() {
		delight_me( 'enqueue_flawless_css_control', $this );
		sps_enqueue_codemirror();
	}

	public function render_content() {
	delight_me( 'render_flawless_css_control', $this );
		$data = $this->value( 'custom_css' );

		if ( ! is_array( $data ) || empty( $data ) ) {
			$data = array(
				0 => array(
					'group' => 'main',
					'css'   => (string) $data
				)
			);
		}

		$selected = apply_filters( 'flawless_css_group_selected', 0, 1 );
		$el = new Super_HTML_Gen();

		$el->render();

		$el->create( 'div' )
		   ->data( 'flawless-css-pane', 1, 'string' );

		$el->create( 'label' );

		$el->create( 'span' )
		   ->add_class( 'customize-control-title' )
		   ->inner( __( 'Page Specific CSS' ) )
		   ->close( 'span' );

		$el->create( 'span' )
		   ->add_class( 'description customize-control-description' )
		   ->inner( __( 'Write plain css or less' ) )
		   ->close( 'span' );


		$el->close( 'label' );

		$field_id = $this->id;
		$el->create( 'button' )
		   ->add_class( 'flawless-open-editor flawless-css-editor button-secondary' )
		   ->data( 'data-customize-setting-link', $field_id, 'string' )
		   ->text( __( 'Open CSS editor' ) );


		$el->render();

		$test = new Super_HTML_Gen();
	}

}