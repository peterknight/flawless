<?php

class Flawless_Customize_CSS_Optimized_Control extends WP_Customize_Control {
	//public $type = 'sps-custom-css';

	/*public function to_json() {
		parent::to_json();
		$this->json['link']    = $this->get_link();
		$this->json['value']   = $this->value();
	}*/

	public function enqueue() {
		sps_enqueue_codemirror();
	}

	public function render_content() {

		$data = $this->value( 'optimized-css');

		if ( empty( $data ) ) {
			$data = '';
		}

		$el = new Super_HTML_Gen();

		$el->render();

		$el->create( 'div' )
		   ->data( 'flawless-css-pane', 1, 'string' )
		   ->data( 'sps-control-id', $this->id );

		$el->create( 'label' );

		$el->create( 'span' )
		   ->add_class( 'customize-control-title' )
		   ->inner( __( 'Optimized CSS' ) )
		   ->close( 'span' );

		/*$el->create( 'span' )
		   ->add_class( 'description customize-control-description' )
		   ->inner( __( 'View Optimized CSS' ) )
		   ->close( 'span' );*/


		$el->close( 'label' );

		$field_id = $this->id;
		$el->create( 'button' )
		   ->add_class( 'flawless-open-editor flawless-css-editor button-secondary' )
		   ->data( 'data-customize-setting-link', $field_id, 'string' )
		   ->text( __( 'View Optimized CSS' ) );


		$el->render();

		$test = new Super_HTML_Gen();
	}

}