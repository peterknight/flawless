 <?php

class SPS_Customize_Var_Group extends WP_Customize_Control {

	public $type = 'var-group';
/*
	public function __construct( $manager, $id, $args = array() ) {

		parent::__construct( $manager, $id, $args );
	}

	public function enqueue() {

	}*/

	public function render_content() {

		$el = new Super_HTML_Gen();

		//$el->create( 'div' );

		$el->create('label')
			->add_class('customize-control');

		$el->create('span')
			->add_class('customize-control-title')
			->text( $this->label )->close('span');

		if( isset( $this->description ) ) {
			$el->create( 'span' )
			   ->add_class( 'customize-control-description' )
			   ->text( $this->description );
		}
		$el->render();
	}
}