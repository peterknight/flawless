(function ( api, $, _o ) {
    var compiledLess = '';
    var cssControls = {};
    window.cssControls = cssControls;
    var $saveCompile;
    var initialisedWorkspacePanels = false;

    var cmLoader =  {
        defaultAddons: [],
        getModeAddon: function ( mode ) {
            if( mode === 'less' || mode === 'text/x-less' ){
                mode = 'css';
            }
            return 'sps-codemirror/mode/' + mode + '/' + mode + '.js';
        },
        addonsToLoad: function ( mode ) {
            return Promise.all(
                _.union( this.defaultAddons, [ this.getModeAddon( mode ) ] )
                    .map( function ( addon ) {
                        console.log( 'ADDING ADDON', addon );
                        return System.import( addon );
                    } )
            );
        },

        isCodeMirrorMode: function ( mode ) {
            switch ( mode ) {
                case 'css':
                case 'less':
                case 'text/x-less':
                case 'htmlmixed':
                case 'javascript':
                case 'js':
                case 'html':
                    return true;
                default:
                    return false;
            }
        },
        loadCMAssets: function () {

            return _o.load( 'sps-codemirror' )
                .then( function () {
                        return _o.load( 'sps-codemirror-theme' );
                    }
                ).then( function() {
                    return _o.load('codemirror-colorpicker');
                });
        },
        load: function(mode){
            return Promise.resolve(true);
            return cmLoader.loadCMAssets().then( function () {
                return cmLoader.addonsToLoad( mode );
            });
        }

    };



    function maybeInitPanels(){
        return new Promise( function( resolve ){
            if( ! initialisedWorkspacePanels ){
                return cmLoader.load('less').then(function(){
                    WorkspacePanelManager.mount();
                    return resolve();
                    console.log('using new mount method success');
                } );
                console.log('using new mount method');
            } else {
                return Promise.resolve();
            }
        } );
    }

    function cssNeedsRecompiling( recompile ){
        if( recompile ){
            $saveCompile.show();
        } else {
            $saveCompile.hide();
        }
    }
    compilationStatus = function(){

        /*
        If custom_css or any of the variable fields is updated, need to update compilation status
         */
        fields = [];
        fields.forEach( function(field) {
            api( field, function ( value ) {
                value.bind( function ( newVal, oldValue ) {
                    devNeedsUpdating( true );
                } );
            } );
        });
        /*
        if Dev updated or if not in sync with compiled, show that we can recompile
         */
        versionData = cssControls.optimizedCSS[versionField].get();
        api( versionField, function ( value ) {
            value.bind( function ( newVal, oldValue ) {
                if( newVal.devVersion !== newVal.compiledVersion ){
                    cssNeedsRecompiling( true );
                } else {
                    cssNeedsRecompiling( false );
                }
            } );
        } );


    };

    api.bind( 'saved', function( response ){
        if( response['flawless_css_version_status'] ){
            //alert('bingo');
            var versionStatus = cssControls.optimizedCSS.settings['version-status'].get();
            console.log('UPDATINGVERSIONSTATUS', versionStatus);
            versionStatus['production_version'] = response['flawless_css_version_status']['production_version'];
            cssControls.optimizedCSS.settings['version-status'].set( versionStatus );
           /* cssControls.optimizedCSS.settings['compiled-version'].set(
                response['flawless_css_version_status']['production_version']
            );*/
            $saveCompile.hide();
        }
        console.log('saved', response);
    });

    api.bind( 'ready', function () {
        console.log( 'komaaan' );

        init();

        $saveButton = $( '#save' );

        $refreshButton = $('<span class="dashicons dashicons-update"></span>')
            .attr( "id", 'refresh-the-preview' )
            .attr( "name", 'refresh-the-preview' )
            .css( { "margin" : '5px' } )
            .addClass('dashicons dashicons-update');
        ;

        $refreshButton.on("click", function(e){
            e.preventDefault();
            api.previewer.refresh();
        });


        $saveCompile = $saveButton
            .clone()
            .attr( "id", 'save-compile' )
            .attr( "name", 'save-compile' )
            .css( { "margin-right" : '10px' } )
            .val( 'Compile CSS' );

        $saveCompile.on( "click", function ( e ) {
            e.preventDefault();

            _o.load('fsc-ui').then( function(){
                var compiledLess = cssControls.compiledCSS.setting.get();
                console.log( 'compiling...' );
                fscApi.compile( compiledLess, 'clean' ).then( function ( result ) {
                    console.log( 'finished', result );
                    //cssControls.optimizedCSS.setting.set( result.css );
                    cssControls.optimizedCSS.settings['optimized-css'].set( result.css );

                    //api.previewer.save(); // for some reason this doesn't submit the changes
                    $saveButton.trigger('click'); // but this does
                } );
                /*fscApi.compile( compiledLess, 'optimized' ).then( function ( result ) {
                    console.log( 'finished', result );
                    cssControls.compiledCSS.setting.set( result );
                } );*/

            });


        } );

        $saveCompile.insertBefore( $saveButton );
        $refreshButton.insertBefore( $saveButton );


    } );
    function devNeedsUpdating( update ){
        if( update ) {
            $saveCompile.removeAttr( "disabled" );
        }

    }

    //init();

    function init() {
        api.previewer.bind( 'flawless-compiled-less',
            function ( css ) {
                console.log( 'WORKED', css );
                compiledLess = css;
                cssControls.compiledCSS.setting.set( css );

                devNeedsUpdating( true );
            } );

        api.previewer.bind( 'flawless-compiled-less-error',
            function ( e ) {
                console.log( 'INVALID CSS', e );
            } );

        _o.publishWhen( 'codemirror-ready', function () {
                return (
                    _o.hasLoaded( 'flawless-codemirror', 'script' )
                );
            }, 1000
        );
/*
        _o.onceAfter( 'codemirror-ready', function () {
            _o.onEach(
                '.flawless-codemirror',
                flawlessCodemirrorTextareaActivate,
                {},
                'body',
                false
            );
        } );*/


    }


    var initCSSed = function(control){
        maybeInitPanels().then( function(){

            var panelConfig = {
                mode: 'less',
                type: 'tabbed-panel',
                use: 'codemirror',
                indexField: 'group',
                contentField: 'css',
                editor: flCodemirror,
                config: {
                    mode: 'text/x-less',
                    theme: 'monokai',
                    lineNumbers: false,
                    viewportMargin: Infinity,
                    colorpicker : {
                        mode : 'edit',
                        onChange : function (cm, event) {
                            console.log('onchangecolorpicker',cm, event, this);
                        }
                    },
                    extraKeys : {
                        // when ctrl+k  keys pressed, color picker is able to open.
                        'Ctrl-K' : function (cm, event) {
                            cm.state.colorpicker.popup_color_picker();
                        }
                    }
                },
            };

            const TabbedWorkSpace = WorkspacePanelManager.registerPanel('css', panelConfig, {
                getter: function(){ return control.setting.get(); },
                setter: function(data) { console.log('settingdata', data);return control.setting.set(data); }
            });
            /*tabbedWorkspaces.registerPanel(
             control.id, control, 'text/x-less',  { indexField: 'group', contentField: 'css' }, true
             );*/


            // convenient access to this control from cssControls
            cssControls.customCSS = control;

            var $button = $( control.container ).find( 'button' );
            console.log('CSS CONTROL READY', $button );
            $button.on("click", function(e){
                e.preventDefault();
                if( $button.hasClass("flawless-open-editor") ){
                    $button.removeClass("flawless-open-editor");
                    $button.addClass("flawless-close-editor");
                    $button.text('Close CSS Editor');
                    console.log( 'codeblockcontrolsettingvalue', control.setting.get(), control.section() )
                    $('body').addClass('workspace-content-editor-pane-open');
                    // tabbedWorkspaces.openPanel( control.id );
                    TabbedWorkSpace.openPanel();
                } else {
                    $button.removeClass("flawless-close-editor");
                    $button.addClass("flawless-open-editor");
                    $button.text('Open CSS Editor');
                }

            });
            _o.onEach( '.super-font-selector', function ( el ) {
                $( el ).on("super-font-loaded", function(e){
                    console.log('new trigger!', e.font );

                    var font = e.font;

                    api.previewer.send( 'flawless-font-request', font );
                    if( font.indexOf(' ') !== -1 ){
                        font = '"' + font + '"';
                    }
                    $(el).prev('input').val( e.font ).trigger('change');
                });
                /* $( el ).on( "change", function () {
                 console.log( 'sending font request', $( this ).val() );
                 $(el).closest('div').find('input').val( $(this).val() ).trigger("change");
                 api.previewer.send( 'flawless-font-request', $( this ).val() );
                 } );*/
            } );

            /*control.setting.bind( function ( newValue, oldValue ) {

             } );*/
        });


    }


    wp.customize.controlConstructor[ 'flawless-custom-css' ] = wp.customize.Control.extend( {
        ready: function () {
            var control = this;
            console.log('CSS CONTROL GETRTING READY', document.readyState);
            if( ! document.readyState ){
                window.onload = function(){
                  initCSSed(control);
                };
            } else {
                initCSSed(control);
            }

        }
    } );

    wp.customize.controlConstructor[ 'flawless-custom-css-optimized' ] = wp.customize.Control.extend( {
        ready: function () {
            var control = this;

            // convenient access to this control from cssControls
            cssControls.optimizedCSS = control;
            
            var $button = $( this.container ).find( 'button' );
            console.log('OPTIMIZED CSS CONTROL READY');
            $button.on("click", function(e){
                e.preventDefault();
                if( $button.hasClass("flawless-open-editor") ){
                    $button.removeClass("flawless-open-editor");
                    $button.addClass("flawless-close-editor");
                    $button.text('Close CSS Editor');
                    console.log( 'codeblockcontrolsettingvalue', control.setting.get(), control.section() )
                    $('body').addClass('flawless-content-editor-pane-open');
                    tabbedWorkspaces.add( control, 'text/x-less', {} );
                } else {
                    $button.removeClass("flawless-close-editor");
                    $button.addClass("flawless-open-editor");
                    $button.text('Open CSS Editor');
                }

            });

          /*  control.setting.bind( function ( newValue, oldValue ) {

            } );*/
        }
    } );

    wp.customize.controlConstructor[ 'flawless-custom-css-compiled' ] = wp.customize.Control.extend( {
        ready: function () {
            var control = this;
            
            // convenient access to this control from cssControls
            cssControls.compiledCSS = control;
            
            var $button = $( this.container ).find( 'button' );
            console.log('CSS CONTROL READY', $button);
            $button.on("click", function(e){
                e.preventDefault();
                if( $button.hasClass("flawless-open-editor") ){
                    $button.removeClass("flawless-open-editor");
                    $button.addClass("flawless-close-editor");
                    $button.text('Close Compiled CSS View');
                    console.log( 'codeblockcontrolsettingvalue', control.setting.get(), control.section() )
                    $('body').addClass('flawless-content-editor-pane-open');
                    tabbedWorkspaces.add( control, 'less', {} );
                } else {
                    $button.removeClass("flawless-close-editor");
                    $button.addClass("flawless-open-editor");
                    $button.text('View Compiled CSS');
                }

            });

            /*  control.setting.bind( function ( newValue, oldValue ) {

             } );*/
        }
    } );

    function flawlessCodemirrorTextareaActivate( el ) {
        flawlessCodemirrorAddons().then( function () {

            var $el = jQuery( el );
            window.$ya = $el;
            var name = $el.data( "flawless-field-id" );
            var mode = 'text/x-less';

            console.log( name );

            flawlessCodemirrorTextareaActivate.prototype.editors[ name ] = CodeMirror.fromTextArea( el, {
                theme: 'monokai',
                mode: mode,
                lineNumbers: false,
                viewportMargin: Infinity,
                colorpicker : {
                    mode : 'edit',
                    onChange : function (c) {
                        console.log('change', c);
                    }
                },
                extraKeys: {
                    "F11": function ( cm ) {
                        setFullScreen( cm, !isFullScreen( cm ) );
                    },
                    "Esc": function ( cm ) {
                        if ( isFullScreen( cm ) ) setFullScreen( cm, false );
                    }
                }
            } );

            flawlessCodemirrorTextareaActivate.prototype.editors[ name ].setValue( $el.val() );
            flawlessCodemirrorTextareaActivate.prototype.editors[ name ].refresh();
            flawlessCodemirrorTextareaActivate.prototype.editors[ name ].on( 'keyup', function ( cm, ch ) {
                handleUpdate( cm, ch );
            } );
            flawlessCodemirrorTextareaActivate.prototype.editors[ name ].on( 'copy', function ( cm, ch ) {
                handleUpdate( cm, ch );
            } );
            flawlessCodemirrorTextareaActivate.prototype.editors[ name ].on( 'paste', function ( cm, ch ) {
                handleUpdate( cm, ch );
            } );

            function handleUpdate( cm, ch ) {
                console.log( 'codemirrorchange' );
                $el
                    .val( cm.getValue() )
                    .trigger( "change" );
            }

        } );


    }

    flawlessCodemirrorTextareaActivate.prototype.editors = {};
    flawlessCodemirrorTextareaActivate.prototype.addons = false;

    function flawlessCodeMirrorInstance( name ) {
        if ( !_o.isDefined( flawlessCodemirrorTextareaActivate.prototype, 'editors', name ) ) {
            return false;
        }
        return flawlessCodemirrorTextareaActivate.prototype.editors[ name ];
    }

    function doMutation( data, index, css, group ) {
        var newData = JSON.parse( JSON.stringify( data ) );
        console.log( data, index, css, group );
        if ( _o.isDefined( newData, index ) ) {
            if ( newData[ index ].group === group ) {
                newData[ index ].css = css;
            }
        } else {
            if( ! newData ) {
                newData = [];
            }
            newData.push( { 'group': group, 'css': css } );
        }
        console.log( newData );
        return newData;
    }


    function flawlessCodemirrorAddons() {
        return Promise.all( [
            System.import( 'sps-codemirror/mode/css/css.js' ),
        ] );
    }

    var cssVariables = {
        variables: {},
        set: function ( variable, value ) {

        },
        save: function () {
            //control.
        }

    };
  /*  jQuery(document).ready(function(){
        //_o.onceAfter( 'superfontassetsready', function () {
        _o.onEach( '.super-font-selector', function ( el ) {
            $( el ).on("super-font-loaded", function(e){
                console.log('new trigger!', e.font );
                var font = e.font;
                if( font.indexOf(' ') !== -1 ){
                    font = '"' + font + '"';
                }
                $(el).prev('input').val( font ).trigger('change');
            });
        } );
        // } );
    });*/

})( wp.customize, jQuery, onDemandUtil );