<?php

class Flawless_Customize_CSS_Compiled_Control extends WP_Customize_Control {
	//public $type = 'sps-custom-css';

	/*public function to_json() {
		parent::to_json();
		$this->json['link']    = $this->get_link();
		$this->json['value']   = $this->value();
	}*/

	public function enqueue() {
		sps_enqueue_codemirror();
	}

	public function render_content() {

		$data = $this->value();

		if ( empty( $data ) ) {
			$data = '';
		}

		$el = new Super_HTML_Gen();

		$el->render();

		$el->create( 'div' )
		   ->data( 'flawless-css-pane', 1, 'string' );

		$el->create( 'label' );

		$el->create( 'span' )
		   ->add_class( 'customize-control-title' )
		   ->inner( __( 'Compiled CSS' ) )
		   ->close( 'span' );

	/*	$el->create( 'span' )
		   ->add_class( 'description customize-control-description' )
		   ->inner( __( 'View compiled CSS' ) )
		   ->close( 'span' );*/


		$el->close( 'label' );

		$field_id = $this->id;
		$el->create( 'button' )
		   ->add_class( 'sps-open-editor flawless-css-editor button-secondary' )
		   ->data( 'data-customize-setting-link', $field_id, 'string' )
		   ->text( __( 'View CSS' ) );


		$el->render();

		$test = new Super_HTML_Gen();
	}

}