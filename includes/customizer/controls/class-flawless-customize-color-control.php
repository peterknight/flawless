<?php



class Flawless_Customize_Color_Control extends WP_Customize_Color_Control {
	public function __construct( $manager, $id, $args = array() ) {
		/*$this->setting = new StdClass();
		$this->setting->default = '';*/
		parent::__construct( $manager, $id, $args );
	}

	public function to_json() {
		$this->json['statuses'] = $this->statuses;
		$this->json['defaultValue'] = '#3333333';
		$this->json['mode'] = $this->mode;
	}
}