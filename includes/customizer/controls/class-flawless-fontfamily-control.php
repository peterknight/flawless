<?php

class Flawless_FontFamily_Control extends WP_Customize_Control {

	public $type = 'font-select';

	function enqueue() {
		super_font_selector_enqueue();
	}

	public function render_content() {

		$el = new Super_HTML_Gen();

		$el->create( 'div' )
		   ->add_class( 'select2-container' );
		$el->create( 'span' )
		   ->text( 'Select a font' );

		/**
		 * We use a proxy with a hidden input to store the selected font
		 * this made it easier to wrap quotes around fonts with spaces in a separate step without
		 * modifying the font selection script directly.
		 * (This way we don't pass Abril Fatface but "Abril Fatface", which can be put into css)
		 */
		$el->create( 'input' )
		   ->type( 'hidden' )
			->data( 'customize-setting-link', esc_attr( $this->settings['default']->id ) );;
		//->id( $this->id )
		//->name( $this->id );

		$el->create( 'select' )
		   ->add_class( 'super-font-selector' )
		  ->data( 'proxy-customize-setting-link', esc_attr( $this->settings['default']->id ) );
		$test = $this->value();
		$value_without_quotes = str_replace(array('\'', '"'), '', $this->value());
		$el->create('option')
		   ->value( $value_without_quotes )
		   ->text( $value_without_quotes );

		$el->render();
	}
}