var flawlessCustomizerPreview = (function ( $, api ) {
    window.addEventListener( 'message', function ( e ) {
        console.log( 'received message' );
    } );
    var component = {};

    component.getFieldID = function ( field ) {
        if ( flawlessCustomizeVars.fields[ field ] ) {
            return flawlessCustomizeVars.fields[ field ];
        }
        return false;
    };

    return component;
})( jQuery, wp.customize );

