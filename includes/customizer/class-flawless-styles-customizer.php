<?php

class Flawless_Styles_Customizer {

	public $variables = array();
	public $groups = array();
	public $env = 'flawless';
	public $env_id;
	public $post_id;
	public $focus;

	const ADMIN_JS_SCRIPT_HANDLE = 'flawless-customizer-admin';
	const CUSTOMIZE_JS_SCRIPT_HANDLE = 'flawless-customizer';
	const PREVIEW_JS_SCRIPT_HANDLE = 'flawless-customizer-preview';
	const PREVIEW_POST_NONCE_QUERY_VAR = 'CENV';
	const PREVIEW_POST_NONCE_ACTION = 'flawless-customize';
	public $settings_registered = false;
	function __construct( $env = false ) {
		if ( (string) $env ) {
			$this->env = (string) $env;
		}
		add_action( 'customizer_set_' . $this->env, array( $this, 'hooks' ), 10, 4 );
		//add_action( 'customize_register', array( $this, 'customize_register' ) );
		//add_action( 'customize_register', array( $this, 'has_meta_mutations' ) );
		//add_action( 'wp_ajax_customize_save', array( $this, 'backend_request' ) );
		delight_me( 'isajax', wp_doing_ajax() );
		$this->settings_hooks();
	}

	function hooks( $wp_customize, $env_id, $post_id, $focus = '' ) {
		//flawless_styles_meta( 'flawless_stylesheet');
		$this->env_id  = $env_id;
		$this->post_id = $post_id;
		$this->focus   = $focus;
		if( wp_doing_ajax() ){
			delight_me( 'isajaxonhooks', wp_doing_ajax() );
		}
		if ( empty( $this->env_id ) ) {
			wp_die( __( 'Cannot load this customizer screen without specifying a place to store the settings' ) );
		}
		delight_me( 'hooks', get_defined_vars() );
		$this->customize_register();
		//add_action( 'customize_register', array( $this, 'customize_register' ) );
		/*add_action( 'sps_customize_controls', 'sps_css_variable_controls', 10, 2 );
		add_action( 'sps_customize_register_settings', 'sps_css_variable_settings', 10, 2 );*/
		add_action( 'customize_controls_init', array( $this, 'controls_request' ) );
		add_action( 'customize_preview_init', array( $this, 'preview_request' ) );
	}

	function customize_register(){
		if( $this->settings_registered ){
			return;
		}

		flawless_styles_meta( 'flawless_stylesheet');

		$this->settings_hooks();
		$this->register_ui();
		$this->settings_registered = true;
		/*if ( ! empty( $this->ids_to_process ) ) {
			global $wp_customize;
			foreach ( $this->ids_to_process as $id ) {

				$this->register_settings( $id );
				do_action( $this->env . '_customize_pre_register_settings', $wp_customize, $id );
				do_action( $this->env . '_customize_register_settings', $wp_customize, $id );
			}
		}*/
	}

	function backend_request() {
		// need to register settings
		//$this->register_settings();
		//$this->settings_hooks();
		$this->has_meta_mutations();

		if ( ! empty( $this->ids_to_process ) ) {
			global $wp_customize;
			foreach ( $this->ids_to_process as $id ) {

				do_action( $this->env . '_customize_pre_register_settings', $wp_customize, $id );
				$this->register_settings( $id );
				do_action( $this->env . '_customize_register_settings', $wp_customize, $id );
			}
		}

	}

	function preview_request() {
		// register settings, controls/panels/sections and enqueue preview scripts
		//$this->settings_hooks();
		$this->register_ui();
		global $wp_customize;

		$this->enqueue_preview_scripts();
		//flawless_variables_json( $this->env_id );
		require_once( dirname(__FILE__ ) .'/preview.php' );
		do_action( 'flawless_preview_init', $wp_customize, $this->env_id, $this );
		delight_me( 'preview_init', $wp_customize );
	}

	function controls_request() {
		// register settings, controls/panels/sections and enqueue control scripts
		//$this->settings_hooks();
		$this->register_ui();
		global $wp_customize;

		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_customize_scripts' ) );


	}

	function ajax_init() {
		// need to register settings
		//$this->backend_request();
		$this->settings_hooks();
	}

	function ajax_save(){
		delight_me('ajax_save', true);
		$this->backend_request();
	}

	function register_settings( $id = false, $context = 'backend' ) {
		global $wp_customize;
		delight_me( 'ajax', wp_doing_ajax(), $wp_customize );
		if( $id ){
			$this->env_id = $id;
			$this->post_id = $id;
		}

		do_action( $this->env . '_customize_pre_register_settings', $wp_customize, $id );

		do_action( 'flawless_settings_registration', $wp_customize, $this->env_id, $this );

		do_action( 'customize_settings_for_' . $this->env, $this->env_id, $this->env_id, $context );
	}

	function register_ui() {
		global $wp_customize;

		$this->register_scripts();

		flawless_custom_css_control_registration( $wp_customize );

		//$this->register_default_controls( $wp_customize );

		$wp_customize->add_panel( 'flawless-panel',
			array(
				'title'           => 'Flawless Styles',
				'priority'        => 100,
				//'active_callback' => 'sps_is_standalone_page_customize_activated'
				'active_callback' => '__return_true'
			)
		);
		do_action( 'flawless_controls_registration', $wp_customize, $this->env_id, $this );
	}

	function settings_hooks() {
		add_filter( 'map_meta_cap', array( $this, 'filter_map_meta_cap' ), 10, 3 );
		add_filter( 'customize_dynamic_settings_args', array( $this, 'filter_dynamic_settings_args', 10, 2 ) );
		add_filter( 'customize_dynamic_setting_class', array( $this, 'filter_dynamic_setting_class' ), 10, 3 );
		//if( $this->env !== 'flawless' ){
		//add_action( $this->env . '_customize_register_settings', array( $this, 'do_register_settings' ), 10, 3 );
		//}
	}

	function do_register_settings( $wp_customize, $post_id, $context = false){
		do_action( 'flawless_settings_registration', $wp_customize, $post_id, $context );
	}

	function has_meta_mutations() {
		delight_me( 'meta_mut', $_POST, $_REQUEST, $GLOBALS['wp_actions'] );
		if ( isset( $_POST['customize_changeset_data'] ) ) {
			$mutations = json_decode( wp_unslash( $_POST['customize_changeset_data'] ), true );
		} else if ( isset( $_POST['customized'] ) ) {
			$mutations = json_decode( wp_unslash( $_POST['customized'] ), true );
		} else {
			return;
		}

		if ( is_array( $mutations ) ) {
			$ids = array();
			// require_once( 'class-sps-customize-setting.php' );

			foreach ( $mutations as $key => $data ) {

				if ( strpos( $key, 'sps_meta' ) === 0 ) {
					$args = SPS_Customize_Setting::parse_setting_id( $key );

					delight_me( 'has_meta_mutations', get_defined_vars() );

					if ( ! $args || (int) $args['post_id'] < 1 ) {

					}

					$id         = (int) $args['post_id'];
					$ids[ $id ] = $id;

				} else if ( strpos( $key, 'sps_post_setting' ) === 0 ) {
					$args = SPS_Customize_Post_Setting::parse_setting_id( $key );

					delight_me( 'has_meta_mutations_post', get_defined_vars() );

					$id         = (int) $args['post_id'];
					$ids[ $id ] = $id;
				}
			}

			$this->ids_to_process = $ids;


			//$this->backend_request();
			/*if ( ! empty( $ids ) ) {
				global $wp_customize;
				foreach ( $ids as $id ) {

					do_action( $this->env . '_customize_pre_register_settings', $wp_customize, $id );
					$this->register_settings( $id );
					do_action( $this->env . '_customize_register_settings', $wp_customize, $id );
				}
			}*/
		}
	}

	function filter_map_meta_cap( $caps, $cap, $user_id ) {
		if ( preg_match( '/^edit_post_meta\[\d+/', $cap ) ) {
			$keys              = explode( '[', str_replace( ']', '', $cap ) );
			$map_meta_cap_args = array(
				array_shift( $keys ),
				$user_id,
				intval( array_shift( $keys ) ),
				array_shift( $keys ),
			);
			$caps              = call_user_func_array( 'map_meta_cap', $map_meta_cap_args );

			return array( 'edit_pages' );
		}
		return $caps;
	}

	/**
	 * Need to tell the API what Setting Class to use for our postmeta connected fields
	 */
	function filter_dynamic_setting_class( $setting_class, $setting_id, $setting_args ) {

		unset( $setting_id ); // Unused.
		if ( isset( $setting_args['type'] ) && 'sps_meta' === $setting_args['type'] ) {
			$setting_class = 'SPS_Customize_Setting';
		}

		return $setting_class;
	}

	function filter_dynamic_settings_args( $setting_args, $setting_id ) {
		$parsed_setting_id = SPS_Customize_Setting::parse_setting_id( $setting_id );
		//$parsed_neseted_setting_id = SPS_Customize_Nested_Setting::parse_setting_id( $setting_id );
		if ( false === $parsed_setting_id ) {
			return $setting_args;
		}
		$post = get_post( $parsed_setting_id['post_id'] );
		if ( ! $post || ! post_type_exists( $post->post_type ) ) {
			return $setting_args;
		}
		if ( false === $setting_args ) {
			$setting_args = array();
		}
		$setting_args = array_merge( $setting_args, array(
			'type'              => 'sps_meta', // See wpse_257322_filter_dynamic_setting_class().
			'transport'         => 'postMessage',
			'default'           => false,
			'sanitize_callback' => function ( $value ) {
				return $value;
			},
		) );
		return $setting_args;
	}


	function add_variable( $field, $variable, $group = false ) {
		$this->variables[ $field ] = array( $variable, $group );
	}

	function add_group( $group, $field ) {
		if ( ! in_array( $group, $this->groups ) ) {
			$this->groups[ $group ] = $field;
		}
	}

	function get_variables() {
		return $this->variables;
	}

	function get_groups() {
		return $this->groups;
	}


	function register_scripts() {
		wp_register_script(
			self::ADMIN_JS_SCRIPT_HANDLE,
			plugins_url( '/flawless-customizer-admin.js', __FILE__ ),
			array(),
			'1.0'
		);

		wp_register_script(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			plugins_url( "/flawless-customizer.js", __FILE__ ),
			array( 'jquery', 'customize-controls' ),
			'1.0'
		);

		wp_register_style(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			plugins_url( "/flawless-customizer.css", __FILE__ ),
			array( 'customize-controls' ),
			'1.0'
		);

		wp_register_script(
			self::PREVIEW_JS_SCRIPT_HANDLE,
			plugins_url( "/flawless-customizer-preview.js", __FILE__ ),
			array( 'jquery', 'customize-preview' ),
			'1.0'
		);

		do_action( 'sps_customize_assets' );
	}

	function localize_preview_vars() {
		$args           = array();
		$args['fields'] = $this->field_vars();
		//$args['post_id'] = $this->post_id();
		$args['post_id'] = $this->env_id;
		wp_localize_script(
			self::PREVIEW_JS_SCRIPT_HANDLE,
			'flawlessCustomizeVars',
			$args
		);
		delight_me( 'localized', $args );
	}

	function field_vars() {
		return apply_filters(
			'flawless_customize_fields',
			array(),
			//$this->post_id(),
			$this->env_id,
			$this->post()
		);
	}

	function post() {
		return get_post( $this->env_id );
	}

	function post_id() {
		return $this->post_id;
	}

	function localize_controls_vars() {

		$args = apply_filters(
			'flawless_customizer_vars',
			array(
				'cssVars' => $this->get_variables(),
				'groups'  => $this->get_groups(),
				'post_id' => $this->post_id(),
				'fields'  => $this->field_vars()
			),
			$this->post_id(),
			$this->post()

		);

		wp_localize_script(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			'flawlessCustomizeVars',
			$args
		);
	}

	/**
	 * Enqueue scripts for Customizer opened from post edit screen.
	 */
	public function enqueue_customize_scripts() {
		/*if ( ! $this->can_load_customize_post_preview() ) {
			return;
		}*/

		$post = get_post( $this->env_id );
		delight_me( 'enqueue_customize_scripts', $post );
		if ( ! $post ) {
			return;
		}
		wp_enqueue_script( self::CUSTOMIZE_JS_SCRIPT_HANDLE );
		wp_enqueue_style( self::CUSTOMIZE_JS_SCRIPT_HANDLE );
		$data = array(
			'previewed_post' => $post->to_array(),
		);

		wp_scripts()->add_data(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			'data',
			sprintf(
				'var _editPostPreviewCustomizeExports = %s;',
				wp_json_encode( $data )
			)
		);

		$args = array(
			'queryParamName'  => self::PREVIEW_POST_NONCE_ACTION,
			'queryParamValue' => self::PREVIEW_POST_NONCE_QUERY_VAR,
		);

		wp_add_inline_script(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			sprintf(
				'flawlessCustomizer.init( %s );',
				wp_json_encode( $args )
			),
			'after'
		);



		global $wp_customize;

		do_action( 'flawless_controls_init', $wp_customize, $this->post_id, $this );
		//$this->localize_preview_vars();

		$this->localize_controls_vars();
		workspace_enqueue_panel();
	}

	function enqueue_preview_scripts() {

		global $wp_customize;

		wp_enqueue_script( self::PREVIEW_JS_SCRIPT_HANDLE );
		$this->localize_preview_vars();
		delight_me( 'komaanpreview', self::PREVIEW_JS_SCRIPT_HANDLE);
	}

}
