<?php


add_action ( 'flawless_settings_registration', 'flawless_sheet_options_settings_reg', 10, 3 ); // $wp_customize, object_id
//add_action ( 'flawless_settings_registration', 'flawless_nested_css_setting_registration', 10, 3 );
add_action ( 'flawless_controls_registration', 'flawless_sheet_options_controls', 10, 3 );

function flawless_sheet_options_settings_reg( $wp_customize, $post_id, $flawless ){
	$settings = array();
	$id = SPS_Customize_Setting::create_setting_id(
		$post_id, '_flawless_routes'
	);
	$settings[ 'flawless_routes' ] = $wp_customize->add_setting(
		new SPS_Customize_Setting(
			$wp_customize,
			$id,
			array(
				'post_id'   => $post_id,
				'meta_key'  => '_flawless_routes',
				'transport' => 'postMessage',
			)
		)
	);

	$settings[ 'flawless_stylesheet_name' ] = $wp_customize->add_setting(
		new SPS_Customize_Post_Setting(
			$wp_customize,
			SPS_Customize_Post_Setting::create_setting_id( $post_id, 'post_title' ),
			array(
				'post_id'   => $post_id,
				'setting'  => 'post_title',
				'transport' => 'postMessage',
			)
		)
	);

	$settings[ 'flawless_stylesheet_status' ] = $wp_customize->add_setting(
		new SPS_Customize_Post_Setting(
			$wp_customize,
			SPS_Customize_Post_Setting::create_setting_id( $post_id, 'post_status' ),
			array(
				'post_id'   => $post_id,
				'setting'  => 'post_status',
				'transport' => 'postMessage',
			)
		)
	);
	return $settings;
}

function flawless_sheet_options_controls($wp_customize, $post_id, $flawless){

	$settings = flawless_sheet_options_settings_reg( $wp_customize, $post_id, $flawless );

	$args = array(
		'title'           => 'Stylsheet Settings',
		'panel'           => 'flawless-panel',
		'capability'      => 'edit_posts',
		'active_callback' => '__return_true'
	);

	$section = $wp_customize->add_section( "flawless_sheet_settings", $args );

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'stylesheet_route',
			array(
				'label'          => __( 'Route to match', 'theme_name' ),
				'section'        => $section->id,
				'settings'       => $settings['flawless_routes']->id,
				'type'           => 'text',
				'default'        => '/'
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'stylesheet_status',
			array(
				'label'          => __( 'Stylesheet status', 'theme_name' ),
				'section'        => $section->id,
				'settings'       => $settings['flawless_stylesheet_status']->id,
				'type'           => 'radio',
				'choices'        => array(
					'draft'   => __( 'Draft' ),
					'publish'  => __( 'Publish' )
				),
				'default'        => 'draft'
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'stylesheet_name',
			array(
				'label'          => __( 'Stylesheet name', 'theme_name' ),
				'section'        => $section->id,
				'settings'       => $settings['flawless_stylesheet_name']->id,
				'type'           => 'text',
				'default'        => ''
			)
		)
	);
}