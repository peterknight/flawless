<?php
require_once( 'customizer-css-actions.php');

add_action( 'flawless_settings_registration', 'flawless_customize_css_settings_registration', 10, 2 ); // $wp_customize, object_id


add_action( 'flawless_controls_registration', 'flawless_custom_css_controls', 10, 3 );
function flawless_custom_css_controls( $wp_customize, $post_id, $focus = false ) {


	//$setting = flawless_customize_css_settings_registration( $wp_customize, $post_id );
	$settings         = flawless_customize_css_settings_registration( $wp_customize, $post_id );
	$version_settings = flawless_version_settings( $wp_customize, $post_id );
	// Add the custom objects for Custom CSS on both the customizer frame
	// and the preview frame.
	$section = $wp_customize->add_section( 'flawless_custom_css', array(
		'title'           => __( 'Custom CSS' ),
		//'description'     => __( 'come on' ),
		'panel'           => 'flawless-panel',
		'priority'        => 160,
		'capability'      => 'edit_theme_options',
		// As this should be persistent over any page the preview
		// is loaded, set the active state to true all the time.
		'active_callback' => '__return_true',
	) );


	$wp_customize->add_control( new Flawless_Customize_CSS_Control( $wp_customize, $settings['flawless-custom-css']->id, array(
		'label'           => __( 'Custom CSS for this post' ),
		'type'            => 'flawless-custom-css',
		'section'         => $section->id,
		'active_callback' => '__return_true',
		'settings'        =>  $settings['flawless-custom-css']->id,

	) ) );

	$wp_customize->add_control(
		new Flawless_Customize_CSS_Compiled_Control( $wp_customize, $settings['flawless-custom-css-compiled']->id, array(
			'label'           => __( 'Custom CSS for this post' ),
			'type'            => 'flawless-custom-css-compiled',
			'section'         => $section->id,
			'active_callback' => '__return_true',
			//'settings'        => 'background_color2',
			'settings'        => $settings['flawless-custom-css-compiled']->id
		) ) );

	$wp_customize->add_control(
		new Flawless_Customize_CSS_Optimized_Control( $wp_customize, $settings['flawless-custom-css-optimized']->id, array(
			'label'           => __( 'Custom CSS for this post' ),
			'type'            => 'flawless-custom-css-optimized',
			'section'         => $section->id,
			'active_callback' => '__return_true',
			'settings'        => array(
				'optimized-css'     => $settings['flawless-custom-css-optimized']->id,
				'dev-version'      => $version_settings['flawless-css-dev-version']->id,
				'compiled-version' => $version_settings['flawless-css-production-version']->id,
				'version-status'   => $version_settings['flawless-css-version-status']->id,
			)
		) ) );
}

add_filter( 'flawless_customize_sections', 'flawless_custom_css_section' );

function flawless_custom_css_section( $sections ) {
	$sections[] = 'flawless_custom_css';

	return $sections;
}

function flawless_custom_css_customize_fields( $fields, $post_id ) {
	$fields['_flawless_custom_css']           = array(
		'label'   => 'Custom Page CSS',
		'type'    => 'textarea',
		'default' => array( array( 'group' => 'main', 'css' => 'body {}' ) ),
	);
	$fields['_flawless_custom_css_compiled']  = array(
		'label'   => 'Custom Page CSS',
		'type'    => 'textarea',
		'default' => '',
	);
	$fields['_flawless_custom_css_optimized'] = array(
		'label'   => 'Custom Page CSS optimized',
		'type'    => 'textarea',
		'default' => '',
	);
	return $fields;
}

add_filter( 'flawless_custom_css_settings', 'flawless_custom_css_customize_fields', 10, 2 );

add_filter( 'flawless_customize_fields', 'flawless_css_field_ids', 10, 3 );

function flawless_css_field_ids( $fields, $post_id, $post ) {

	$fields['custom-css'] = SPS_Customize_Setting::create_setting_id(
		$post_id,
		'_flawless_custom_css'
	);

	return $fields;
}

//add_action( 'sps_customize_register_settings', 'sps_customize_css_settings_registration', 10, 2 );
function flawless_customize_css_settings_registration( $wp_customize, $post_id ) {

	$settings = array();

	$settings['flawless-custom-css'] = new SPS_Customize_Setting(
		$wp_customize,
		SPS_Customize_Setting::create_setting_id( $post_id, '_flawless_custom_css' ),
		array(
			'type'            => 'sps_meta',
			'active_callback' => '__return_true',
			'default'         => array( array( 'group' => 'main', 'css' => 'body {}' ) ),
			'transport'       => 'postMessage',
		)
	);

	$settings['flawless-custom-css-compiled'] = new SPS_Customize_Setting(
		$wp_customize,
		SPS_Customize_Setting::create_setting_id( $post_id, '_flawless_custom_css_compiled' ),
		array(
			'type'            => 'sps_meta',
			'active_callback' => '__return_true',
			'default'         => '',
			'transport'       => 'postMessage',
		)
	);

	$settings['flawless-custom-css-optimized'] = new SPS_Customize_Setting(
		$wp_customize,
		SPS_Customize_Setting::create_setting_id( $post_id, '_flawless_custom_css_optimized' ),
		array(
			'type'            => 'sps_meta',
			'active_callback' => '__return_true',
			'default'         => '',
			'transport'       => 'postMessage',
		)
	);

	foreach ( $settings as $setting ) {
		$wp_customize->add_setting( $setting );
	}

	return $settings;
}

add_action( 'flawless_controls_init', 'flawless_customizer_compiler', 10, 2 );
function flawless_customizer_compiler() {
	$compiler = fsc_get_compiler();
	fsc_enqueue_script();
	//wp_enqueue_script( 'fsc-ui' );
}


add_action( 'flawless_controls_registration', 'flawless_add_on_scripts', 9 );

function flawless_add_on_scripts() {
	/*wp_register_script(
		'optimal-select',
		plugins_url( '/optimal-select/optimal-select.min.js', SPS_LIB_PATH ),
		array(),
		'4.0.0'
	);*/

	sps_codemirror_boot();
	sps_codemirror_assets();
}

function flawless_custom_css_assets() {
	wp_register_script(
		'flawless-custom-css',
		plugins_url( 'controls/flawless-custom-css.js', __FILE__ ),
		array( 'underscore', 'customize-preview', 'flawless-customizer-preview' )
	);

	wp_register_script(
		'flawless-custom-css-control',
		plugins_url( 'controls/flawless-custom-css-control.js', __FILE__ ),
		array(
			'underscore',
			'systemjs',
			'on-demand-utils',
			'jquery-ui-sortable',
			'jquery-ui-droppable',
			'customize-controls',
			'flawless-customizer'
		),
		true
	);
}

add_action( 'flawless_controls_registration', 'flawless_custom_css_assets', 9 );

add_action( 'flawless_controls_init', 'flawless_custom_css_control' );

function flawless_custom_css_control() {
	prk_enqueue_script(
		'flawless-custom-css-control',
		array(
			'on'   => 'load',
			'meta' =>
				array(
					'sps-codemirror'              => array(
						// 'scriptImport' => true
						'global' => 'CodeMirror'
					),
					'flawless-custom-css-control' => array(
						'scriptImport' => true, // importing fsc.js errors unless injected into dom
					),
				)
		) );
	//wp_enqueue_script( 'codemirror-colorpicker' );
	//wp_enqueue_script( 'sps-custom-css-control' );
	sps_enqueue_codemirror();
}

add_action( 'flawless_preview_init', 'flawless_custom_css_preview', 10, 2 );

function flawless_custom_css_preview( $wp_customize, $post_id ) {
	delight_me( 'custom_css_preview_enq', $post_id );
	//add_action( 'standalone_page_head', 'sps_custom_css_styles', 9, 1 );
//	add_action( 'wp_head', 'sps_load_theme_less' );
	wp_enqueue_script( 'flawless-custom-css' );
	//prk_enqueue_script( 'sps-custom-css', array( 'on' => 'async' ) );
	$compiler = fsc_get_compiler();
	$compiler->load_lessjs();
}