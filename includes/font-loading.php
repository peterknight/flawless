<?php

require_once( FLAWLESS_STYLES_LIB_PATH . '/flawless-font-loader/boot.php' );

function flawless_enqueue_fonts(){
	$stylesheet_data = flawless_shown_theme();
	if( empty( $stylesheet_data['meta']['_flawless_typography'] ) ){
		return;
	}
	$fonts = flawless_produce_typography_configs( $stylesheet_data );
	delight_me( 'fonts_to_load', $fonts, $stylesheet_data );
	foreach( $fonts as $var_name => $config ){
		if( is_array( $config ) && isset( $config['fontfamily'] ) && ( ! isset( $config['source'] ) || $config['source'] == 'google' ) ){
			flawless_enqueue_google_font( $config['fontfamily'], $config );
		}
	}
}

function flawless_produce_typography_configs( $data ){
	$configs = array();
	foreach( $data['meta']['_flawless_typography'] as $var_name => $font ){
		if( empty( $font ) ){
			continue;
		}
		$configs[] = array( 'source' => 'google', 'fontfamily' => $font );
	}
	return $configs;
}