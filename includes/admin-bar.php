<?php

add_action( 'admin_bar_menu', 'flawless_styles_bar_links', 999 );

function flawless_styles_bar_links( $wp_admin_bar ) {

	global $wp_customize;

	// Don't show for users who can't access the customizer or when in the admin.
	if ( ! current_user_can( 'customize' ) || is_admin() ) {
		return;
	}


	// Don't show if the user cannot edit a given customize_changeset post currently being previewed.
	if ( is_customize_preview() && $wp_customize->changeset_post_id() && ! current_user_can( get_post_type_object( 'customize_changeset' )->cap->edit_post, $wp_customize->changeset_post_id() ) ) {
		return;
	}

	$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	if ( is_customize_preview() && $wp_customize->changeset_uuid() ) {
		$current_url = remove_query_arg( 'customize_changeset_uuid', $current_url );
	}

	$customize_url = add_query_arg( 'url', urlencode( $current_url ), wp_customize_url() );
	if ( is_customize_preview() ) {
		$customize_url = add_query_arg( array( 'changeset_uuid' => $wp_customize->changeset_uuid() ), $customize_url );
	}


	$new_url = $customize_url;



	//add_action( 'wp_before_admin_bar_render', 'sps_customize_support_script' );

	$args = array(
		'id'    => 'flawless-styles',
		'title' => 'Flawless Styles',
		'href'  => $new_url,
		'meta'  => array(
			'class' => 'sps-customizer-boot',
			'title' => 'Flawless Stylesheet'
		)
	);
	$wp_admin_bar->add_node( $args );

	$create_url_args = array(
		'fsc_nonce' => wp_create_nonce( 'flawless-styles-edit' ),
		'customize_url' => urlencode( $current_url )
	);

	$create_url = add_query_arg( $create_url_args, $current_url );


// Add another child link
	$args = array(
		'id'     => 'flawless-styles-create',
		'title'  => 'Create a stylesheet',
		'href'   => $create_url,
		'parent' => 'flawless-styles',
		'meta'   => array(
			'class' => 'flawless-stylesheet-create',
			'title' => 'Create a stylesheet'
		)
	);
	$wp_admin_bar->add_node( $args );

// Add a child link to the child link

	$active_theme = flawless_shown_theme();
	if( $active_theme ) {
		$edit_url = add_query_arg( array(
			'CENV'  => 'flawless',
			'CUIID' => $active_theme['ID']
		), $new_url );

		$args = array(
			'id'     => 'flawless-styles-edit',
			'title'  => 'Edit Stylesheet',
			'href'   => $edit_url,
			'parent' => 'flawless-styles',
			'meta'   => array(
				'class' => 'flawless-stylesheet-edit',
				'title' => 'Edit Stylesheet'
			)
		);
		$wp_admin_bar->add_node( $args );
	}



}

function flawless_styles_new_theme() {

	$nonce = $_GET['fsc_nonce'];

	if ( ! wp_verify_nonce( $nonce, 'flawless-styles-edit' ) ) {
		wp_die( __( 'messed up nonce', 'flawless-styles' ) );
	}

	$destination_url = urldecode( $_GET['customize_url'] );

	$args     = array(
		'post_type'   => 'flawless_stylesheet',
		'post_title'  => 'New theme',
		'post_status' => 'draft'

	);
	$theme_id = wp_insert_post( $args );

	$redirect = flawless_customize_url( $destination_url, $theme_id );
	delight_me( 'new_themne', get_defined_vars() );
	wp_redirect( $redirect );
	exit;
}

function flawless_add_stylesheet_redirect(){
	delight_me( 'req', $_GET );
	if( ! isset( $_GET['fsc_nonce'] ) ){
		return;
	}
	if( ! is_user_logged_in() ){
		return;
	}
	if( ! current_user_can( 'manage_options' ) ){
		return;
	}
	delight_me( 'nt1', $_GET );
	flawless_styles_new_theme();
}
add_action( 'template_redirect', 'flawless_add_stylesheet_redirect' );

function flawless_customize_url( $url, $id ){
	$url = add_query_arg( array(
		'CENV' => 'flawless',
		'CUIID' => (int) $id,
	), $url );

	$customize_url = add_query_arg( array(
			'url' => urlencode( $url ),
			'CENV' => 'flawless',
			'CUIID' => (int) $id,
		),
		wp_customize_url()
	);
	return $customize_url;
}

