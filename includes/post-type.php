<?php

function flawless_styles_post_type() {
	if( did_action( 'flawless_posttype_set') ){
		return;
	}
	$labels = array(
		'name'               => _x( 'Flawless Stylesheet', 'Flawless Stylesheets', 'text_domain' ),
		'singular_name'      => _x( 'Flawless Stylesheet', 'Flawless Stylesheet', 'text_domain' ),
		'menu_name'          => __( 'Flawless Stylesheets', 'text_domain' ),
		'name_admin_bar'     => __( 'Stylesheets', 'text_domain' ),
		'parent_item_colon'  => __( 'Parent Item:', 'text_domain' ),
		'all_items'          => __( 'All Flawless Stylesheets', 'text_domain' ),
		'add_new_item'       => __( 'Compose new Flawless Stylesheet', 'text_domain' ),
		'add_new'            => __( 'Compose new Flawless Stylesheet', 'text_domain' ),
		'new_item'           => __( 'New Flawless Stylesheet', 'text_domain' ),
		'edit_item'          => __( 'Edit Flawless Stylesheet', 'text_domain' ),
		'update_item'        => __( 'Update Flawless Stylesheet', 'text_domain' ),
		'view_item'          => __( 'View Flawless Stylesheet', 'text_domain' ),
		'search_items'       => __( 'Search Flawless Stylesheet', 'text_domain' ),
		'not_found'          => __( 'Not found', 'text_domain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'text_domain' ),
	);
	$args   = array(
		'label'               => __( 'stylesheet', 'text_domain' ),
		'description'         => __( 'Create stylesheet', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions' ),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		//'show_in_menu'        => 'edit.php?post_type=standalone_page',
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-media-default',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array(
			'slug'  => __( 'flawless-stylesheet' ),
			'feeds' => false,
			'pages' => false
		)
	);
	register_post_type( 'flawless_stylesheet', $args );
	do_action( 'flawless_posttype_set' );
}

add_action( 'customizer_set_flawless', 'flawless_styles_post_type', 1 );
add_action( 'init', 'flawless_styles_post_type' );