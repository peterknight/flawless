=== Flawless Styles ===
Contributors: peterknight
Tags: debug, debugging, var_dump, xdebug, log, logger
Requires at least: 3.0
Tested up to: 4.2
Stable tag: 0.1.0
License: GPLv2 or later

A powerful UI for writing and modifying CSS for WordPress themes, individual pages and components.

== Description ==

Write large amounts of CSS with an online editing interface that supports tabs for better organization. Tweak the CSS
of WordPress themes without having to creating a child theme. Work with variables and a UI to provide more intuitive
ways of modifying CSS values (including a font picker, color picker and media selection).

== Installation ==

Upload the Flawless Styles plugin to your WordPress site, then activate it.

== Known issues ==



== Changelog ==

= 0.1.0 =
